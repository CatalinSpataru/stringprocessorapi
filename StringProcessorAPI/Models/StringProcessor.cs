﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StringProcessorAPI.Models
{
    public class StringProcessor
    {
        private List<string> InitialList;
        private List<string> Stage1Processed;
        private List<string> Stage2Processed;
        private List<string> Stage3Processed;
        private List<List<int>> Stages;
        private bool Stage1Finished = false;
        private bool Stage2Finished = false;
        private bool Stage3Finished = false;

        public StringProcessor(List<string> dataToProcess, List<List<int>> stages)
        {
            InitialList = dataToProcess;
            Stage1Processed = new List<string>(InitialList.Count());
            Stage2Processed = new List<string>(InitialList.Count());
            Stage3Processed = new List<string>(InitialList.Count());
            Stages = stages;
        }

        public List<string> Get()
        {
            while (!Stage3Finished) { } //wait for Stage 3 to be finished

            return Stage3Processed;
        }

        public void ExecuteStage1()
        {
            while (InitialList.Count() > 0)
            {
                string currentString = InitialList[0].ToString();
                InitialList.RemoveAt(0);

                currentString = ProcessString(currentString, 0);

                Stage1Processed.Add(currentString);
            }

            Stage1Finished = true;
        }

        public void ExecuteStage2()
        {
            while (Stage1Processed.Count() > 0 || !Stage1Finished)
            {
                if (Stage1Processed.Count() > 0)
                {
                    string currentString = Stage1Processed[0].ToString();
                    Stage1Processed.RemoveAt(0);

                    currentString = ProcessString(currentString, 1);

                    Stage2Processed.Add(currentString);
                }
            }

            Stage2Finished = true;
        }

        public void ExecuteStage3()
        {
            while (Stage2Processed.Count() > 0 || !Stage2Finished)
            {
                if (Stage2Processed.Count() > 0)
                {
                    string currentString = Stage2Processed[0].ToString();
                    Stage2Processed.RemoveAt(0);

                    currentString = ProcessString(currentString, 2);

                    Stage3Processed.Add(currentString);
                }
            }

            Stage3Finished = true;
        }

        private string ProcessString(string str, int stageNumber)
        {
            foreach (int action in Stages[stageNumber])
            {
                switch (action)
                {
                    case 0:
                        str = LowerString(str);
                        break;
                    case 1:
                        str = UpperString(str);
                        break;
                    case 2:
                        str = SortString(str);
                        break;
                    case 3:
                        str = InvertString(str);
                        break;
                    case 4:
                        str = RemoveSpaces(str);
                        break;
                }
            }

            return str;
        }

        private string LowerString(string str)
        {
            return str.ToLower();
        }

        private string UpperString(string str)
        {
            return str.ToUpper();
        }

        private string SortString(string str)
        {
            var listOfChars = str.ToCharArray();

            Array.Sort(listOfChars, Comparer);

            string sortedString = "";
            listOfChars.ToList().ForEach(c => sortedString += (c.ToString()));
                
            return sortedString;
        }

        private string InvertString(string str)
        {
            return str.Reverse().ToString();
        }

        private string RemoveSpaces(string str)
        {
            return str.Replace(" ", "");
        }

        private int Comparer(char a, char b)
        {
            a = a >= 'a' && a <= 'z' ? (char)(a - ('a' - 'A')) : a;
            b = b >= 'a' && b <= 'z' ? (char)(b - ('a' - 'A')) : b;
            return a - b;
        }
    }
}
