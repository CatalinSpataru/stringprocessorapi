﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StringProcessorAPI.Models;

namespace StringProcessorAPI.Controllers
{
    /// <summary>
    /// 0 = Lowercase
    /// 1 = Uppercase
    /// 2 = Sort
    /// 3 = Invert
    /// 4 = Remove Spaces
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class StringProcessorController : ControllerBase
    {
        private static List<string> DataToProcess;
        private static List<List<int>> Stages;
        private static List<string> ProcessedStrings;

        [Route("")]
        [HttpGet]
        public ActionResult Default()
        {
            return Ok();
        }

        [Route("InitializeProcessor")]
        [HttpPut]
        public ActionResult InitializeProcessor(List<string> toProcess)
        {
            DataToProcess = toProcess;
            Stages = Stages != null ? Stages : new List<List<int>>();

            for (int index = 0; index < 3; index++)
            {
                Stages.Add(new List<int>(5));
            }

            return Ok();
        }

        [Route("GetResult")]
        [HttpGet]
        public ActionResult<List<string>> GetResult()
        {
            return ProcessedStrings != null && ProcessedStrings.Count() > 0 ? ProcessedStrings : DataToProcess;
        }

        [Route("StartProcessing")]
        [HttpPost]
        public ActionResult<string> StartProcessing()
        {
            if (DataToProcess == null)
                return Ok("Strings to process are necessary!");

            var stringProcessor = new StringProcessor(DataToProcess, Stages);

            Thread stage1 = new Thread(stringProcessor.ExecuteStage1);
            Thread stage2 = new Thread(stringProcessor.ExecuteStage2);
            Thread stage3 = new Thread(stringProcessor.ExecuteStage3);

            stage1.Start();
            stage2.Start();
            stage3.Start();

            ProcessedStrings = stringProcessor.Get();

            return Ok("Strings have been processed!");
        }

        [Route("GetStagesConfiguration")]
        [HttpGet]
        public ActionResult<List<List<int>>> GetStagesConfiguration()
        {
            return Ok(Stages);
        }

        [Route("AddToStage")]
        [HttpPost]
        public ActionResult AddToStage(int stage, int action)
        {
            if (stage < 3 && action < 5 && Stages[stage].Count() < 4)
            {
                Stages[stage].Add(action);
                return Ok();
            }
            
            return Ok("The provided arguments are not correct!");
        }

        [Route("AddAllStages")]
        [HttpPost]
        public ActionResult AddAllStages(List<List<int>> stages)
        {
            if (stages.Count() < 4)
            {
                bool shapeIsOk = true;

                foreach (var s in stages)
                    if (s.Count() > 5)
                        shapeIsOk = false;

                if (shapeIsOk)
                {
                    Stages = new List<List<int>>();
                    Stages = stages;
                }

                return Ok();
            }
            return Ok("The shape of the array should be arr[3][5] !");
        }

        [Route("RemoveFromStage")]
        [HttpDelete]
        public ActionResult RemoveFromStage(int stage, int action)
        {
            if (stage < 3 && action < 5)
            {
                Stages[stage].Remove(action);
            
                return Ok();
            }
            return Ok("The provided arguments are not correct!");
        }

        [Route("RemoveAllStages")]
        [HttpDelete]
        public ActionResult RemoveAllStages()
        {
            Stages = new List<List<int>>();

            return Ok("Stages cleared!");
        }

    }
}